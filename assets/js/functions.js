function F(){}

F.prototype = 
{
	__construct:function ()
    {
        $(document).ready(function ()
        {
        });
    },

    MejaPesan: function(mejaId, pegawai)
    {
    	window.location.href="/kasir/tagihan_form.php?no="+mejaId+'&pgw='+pegawai;
    },

    pesanMenu: function(menu_id)
    {
        var el = $('input[name="pesan[menu_'+menu_id+'][]"]');

        if(el.val() == null || el.val() == '')
        {
            el.val(1);
            $('#btn-'+menu_id).removeClass('btn-success');
            $('#btn-'+menu_id).addClass('btn-danger');
        }
        else
        {
            el.val('');
            $('#btn-'+menu_id).removeClass('btn-danger');
            $('#btn-'+menu_id).addClass('btn-success');
        }
        return false;
    }
}

var oFunctions = new F();