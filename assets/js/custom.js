function update_element(el )
{
}
$(document).ready(function ()
{
	$('.update-pesanan').on('click',function(){
		var $tr = $(this).parent().parent();
		var elem_menu = $tr.find('div.order-name-menu');
		var elem_menu_hidden = $tr.find('input.nama_menu');

        var elem_qty = $tr.find('div.order-qty');
        var elem_qty_hidden = $tr.find('input.jumlah');

		var replace_name = $('<input type="text" size="15" style="width:auto;" class="form-control" name="nama_menu[]" />'); 
		var replace_qty = $('<input type="text" size="2" style="width:auto;" class="form-control" name="jumlah[]" />'); 
		
		$tr.find('.update-pesanan').hide();
		$tr.find('.proses-update').show();

        elem_menu.hide();
        elem_menu.after(replace_name);

		elem_qty.hide();
		elem_qty_hidden.after(replace_qty);

		replace_name.focus();
        replace_name.blur(function(){
            if(replace_name.val() != null || replace_name.val()=="")
            {
                $(elem_menu_hidden).val(replace_name.val()).change();
                elem_menu.text(replace_name.val());
            }
        });

        replace_qty.focus();
		replace_qty.blur(function(){
			if(replace_qty.val() != null || replace_qty.val()=="")
			{
				$(elem_qty_hidden).val(replace_qty.val()).change();
				elem_qty.text(replace_qty.val());
			}
        });


        $tr.find('.proses-update').click(function()
        {
            replace_qty.remove();
            elem_qty.show();
            replace_name.remove();
            elem_menu.show();

            $(this).hide();
            $tr.find('.update-pesanan').show();
        });
    });
	
    // bayar
    var data = $('#bayar').serialize();
	$('.bayar').click(function()
	{
		$.ajax({
			url : 'proses_order.php',
			type : 'POST',
			dataType : 'json',
			data : data,
			success : function(data){
			    for(var i in data)
			    {
			    	if(data[i] == 0)
			    	{
			    		$('.group-button').hide();
			    		$('.message-failed').show().html('Anda gagal melakukan pembayaran.');
			    		return false;
			    	}
			    }
	    		$('.group-button').hide();
	    		$('.message-ok').show().html('Anda telah melakukan pembayaran.');

			},
			error : function(){
			    console.log('error');
			},
		});
		return false;
	});

	// bagian dapur
	$('.row.dapur').find('.btn').each(function()
	{
	  $(this).on('click',function(){
	    var getMejaId = $(this).attr('id');

	    window.location.href = '/dapur/manage_detail_pesanan.php?meja_no='+getMejaId;
	  });
	});

//utk searching list menu
$('#cari_menu').keyup(function() {
	var hasil = $('#cari_menu').val();
	$.ajax({
		type : 'POST',
		url : 'proses_cari_menu.php',
		dataType : 'json',
		data : 'inputna='+hasil,
		success : function(data) {
			console.log(data);
		}
	});
	return false;
});

});
