<?php session_start(); 
include "lib/templates/header.php";
include "config/koneksi.php";
$order = array();
$no = 0;
?>        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Cafe Teras Sosis
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['customer_name']?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="lib/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">
                
                <h1 style="text-align:center;">Meja $variable_get_meja</h1>
                <!-- Main content -->
                <section class="content">
                
                    <div class="row">
                        <div class="message-ok alert alert-success" style="display:none;">
                        </div>
                        <div class="message-failed alert alert-danger" style="display:none;">
                        </div>
                        <div class="col-xs-12 group-button" style="text-align:center;">
                            
                                <button class="btn btn-lg btn-primary">TAMBAH PESANAN</button><br><br>
                                <form method="post" id="bayar" action="proses_order.php">
                                <?php
                                    foreach($_POST as $index=>$data)
                                    {
                                ?>
                                        <input type="hidden" name="<?php echo $index?>[tanggal]" value="<?php echo date('Y-m-d', time())?>" />
                                        <input type="hidden" name="<?php echo $index?>[quantity]" value="<?php echo $_POST[$index]["jumlah"][0]?>" />
                                        <input type="hidden" name="<?php echo $index?>[id_menu]" value="<?php echo $_POST[$index]["menu_id"][0]?>" />
                                        <input type="hidden" name="<?php echo $index?>[id_pelanggan]" value="<?php echo $_POST[$index]["id_pelanggan"][0]?>" />
                                        <input type="hidden" name="<?php echo $index?>[id_user]" value="<?php echo 1?>" />
                                <?php
                                    }
                                ?>


                                <button class="btn btn-lg btn-danger bayar">BAYAR</button>
                                
                            
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        
        <?php 
include "lib/templates/footer.php";
?>