<?php session_start(); 
if($_SESSION['customer_name'])
{
include "lib/templates/header.php";
include "config/koneksi.php";

$konek = new Koneksi;
$menus = $konek->hasMany('menu','*', 'jenis', 'id_jenis', 'id_jenis');
// simpan data meja,pelanggan, pegawai/waiter
$nama_pegawai = 'tanpa nama';

// masukan data pelanggan ke database
$pelanggan = new Koneksi;
$data_pel = array(
    'nama_pelanggan'=>$_SESSION['customer_name'],
    'create_at'=>time(),
    'id_meja'=>$_SESSION['no_meja']
);

if($pelanggan->insert('pelanggan',$data_pel) ) 
// if(true ) 
{
    $selectPelanggan = new Koneksi;
    $getByName = $selectPelanggan->select('pelanggan','*','nama_pelanggan="'.$_SESSION['customer_name'].'"');
   
    if(isset($getByName))
    {
        $id_pelanggan = $getByName[0]['id_pelanggan'];
    }
    else $id_pelanggan = 0;
    $_SESSION['id_pelanggan'] = $id_pelanggan;
}
else
{
    echo '<pre /><div class="text-danger">Gagal simpan data pelanggan.</div>';
    die;
}
?>        

<!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Cafe Teras Sosis
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['customer_name']?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="lib/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Menu List
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Menu Available</h3>
                                    <div class="box-tools">
                                        <div class="input-group">
                                            <input type="text" name="cari_menu" id="cari_menu" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <form action="proses_menu.php" method="post">
                                    <table class="table table-hover table-bordered" id="tabel_menu">
                                        <tr>
                                            <th colspan="2"><center><h4>Menu</h4></center></th>
                                            <th colspan="2">Jumlah</th>
                                            <!-- <th>Date</th> -->
                                        </tr>
                                        <!-- List Menu -->
                                    <?php 
                                    foreach($menus as $menu)
                                    {
                                    ?>
                                        <tr class="semua_list_menu" id="semua_list_menu">
                                            <td>
                                            	<div>
						                            <!-- small box -->
						                            <div class="small-box bg-aqua">
						                                <div class="inner">
						                                    <h3>
						                                        IDR <?php echo $menu['harga'] ?>
						                                    </h3>
						                                    <p>
						                                        New Orders
						                                    </p>
						                                </div>
						                                <div class="icon">
						                                    <!-- <i class="ion ion-bag"></i> -->
						                                    <img src="<?php echo 'http://'.$menu['gambar'];?>" style="height:6=50px;width:100px;">
						                                </div>
						                                <a href="#" class="small-box-footer">
						                                    More info <i class="fa fa-arrow-circle-right"></i>
						                                </a>
						                            </div>
						                        </div><!-- ./col -->
                                            </td>
                                            <td>
                                            	<p><?php echo $menu['deskripsi'] ?></p>
                                                <p style="align:left;"></p>idr <?php echo $menu['harga'] ?>
                                                <p style="align:right;">stok <?php echo $menu['stok'] ?></p>
                                            </td>
                                            <td class="vert-align">
                                            	<input type="text" name="jumlah[]" class="col-xs-8">

                                                <!-- asdkjasld -->
                                                <input type="hidden" value="" name="pesan[menu_<?php echo $menu['id_menu']?>][]" /> 
                                                <input type="hidden" name="menu_id[]" class="form-control" value="<?php echo $menu['id_menu']?>" />
                                                <input type="hidden" name="nama_pelanggan[]" class="form-control" value="<?php echo $_SESSION['customer_name']?>" />
                                                <input type="hidden" name="id_pelanggan[]" class="form-control" value="<?php echo $id_pelanggan?>" />
                                                <input type="hidden" name="no_meja[]" class="form-control" value="<?php echo $_SESSION['no_meja']?>" />

                                            </td>
                                            <td class="vert-align">
                                            	<div class="btn btn-success" id="btn-<?php echo $menu['id_menu']?>" onclick="oFunctions.pesanMenu(<?php echo $menu['id_menu']?>)">Pesan</div>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                        <!-- End Menu-->
                                        <tr>
                                            <td colspan="4">
                                                <button style="float:right;" class="btn btn-info btn-large" type="submit">Selesai</button>
                                            </td>
                                        </tr>
                                    </table>
                                    </form>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        
        <?php 
include "lib/templates/footer.php";
}
else
{

    $http_host = $_SERVER['HTTP_HOST'];
    $http_port = $_SERVER['SERVER_PORT'];
    // $loc = $http_port != '80' ? $http_host.":".$http_port : $http_host;
    
    header("Location: http://".$http_host."/logout.php");

}
?>