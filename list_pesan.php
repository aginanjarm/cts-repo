<?php session_start(); 
include "lib/templates/header.php";
include "config/koneksi.php";
?>        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Cafe Teras Sosis
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['customer_name']?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="lib/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">
                <h2 style="text-align:center;">List Pesanan</h2>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                <form method="POST" action="after_order.php">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>Pesanan</th>
                                            <th>Jumlah</th>
                                            <th class="table-bordered">Aksi</th>
                                            <!-- <th>Date</th> -->
                                        </tr>
                                    <?php
                                        $ordereds = $_SESSION['ordereds'];
                                        $quantities = $_SESSION['quantities'];
                                            $j = 1;
                                            for($i = 0; $i < count($_SESSION['menus']);$i++)
                                            {

                                                if($ordereds["menu_".$j] != "" && $quantities[$i] != "")
                                                {
                                                    $konek = new Koneksi;
                                                    $menu = $konek->select('menu', '*', 'id_menu='.$j);


                                    ?>
                                        <tr>
                                            <td>
                                            	<?php echo $menu[0]['nama_menu']?>
                                                <input type="hidden" class="nama_id" name="menu_<?php echo $j;?>[menu_id][]" value="<?php echo $menu[0]['id_menu']?>" />
                                            </td>
                                            <td>
                                            	<?php echo $quantities[$i]?>
                                                <input type="hidden" class="jumlah"  name="menu_<?php echo $j;?>[jumlah][]" value="<?php echo $quantities[$i]?>" />
                                            <input type="hidden" class="jumlah"  name="menu_<?php echo $j;?>[pesanan][]" value="<?php echo "menu_".$j?>" />
                                            <input type="hidden" class="nama_pelanggan"  name="menu_<?php echo $j;?>[id_pelanggan][]" value="<?php echo $_SESSION['id_pelanggan']?>" />
                                            </td>
                                            <td class="table-bordered">
                                                <a href="" class="glyphicon glyphicon-remove cancel-pesanan"></a>&nbsp;&nbsp;
                                                <a href="" class="glyphicon glyphicon-ok update-pesanan"></a>
                                                <span class="proses-update" style="display:none;cursor:pointer" id="">Ok</span>
                                            </td>                                        
                                        </tr>
                                        
                                    <?php
                                                }
                                                $j++;
                                            }
                                        
                                    ?>
                                        <tr>
                                            <td colspan="3">
                                                <button style="float:right;" class="btn btn-info btn-large" typ="submit">Proses</button>
                                            </td>
                                        </tr>
                                    </table>
                                    </form>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        
        <?php 
include "lib/templates/footer.php";
?>