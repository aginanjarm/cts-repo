<?php
session_start();
error_reporting(E_ALL);

$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
// $loc = $http_port != '80' ? $http_host.":".$http_port : $http_host;

if(empty($_SESSION['customer_name'])) {
    header("Location: http://".$http_host."/login.php");
} else {
    print_r($_SESSION);
}