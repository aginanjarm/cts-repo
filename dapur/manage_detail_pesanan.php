<?php
// session_start();
include "../lib/templates/header.php";
include "../config/koneksi.php";

$konek = new Koneksi;
$meja_no = $_GET['meja_no'];
$pesanans = $konek->select('pesanan','*','id_meja = '.$meja_no);
?>

<header class="header">
    <a href="/dapur/" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Cafe Teras Sosis
    </a>
    <nav class="navbar navbar-static-top" role="navigation"></nav>
</header>
<div class="container">
	<div class="clear"></div>
	<form id="proses_pesanan" action="" method="POST">
		<div class="jumbotron">
			<?php 
				$i=0;
				foreach ($pesanans as $k => $v): 
			?>
			<div class="row">
			<?php 
				$menu = $konek->select('menu','nama_menu','id_menu='.$v['id_menu']);
				echo "<div class='col-sm-4'>".$menu[0]['nama_menu']."</div>";
				echo "<div class='col-sm-2'>".$v['quantity']."</div>";
				$i++;
			?>
			</div>
			<?php endforeach ?>
		</div>
		<button class="btn btn-primary btn-lg after-order-btn pull-right">Proses</button>
	</form>	
</div>
<?php 
include "../lib/templates/footer.php";
?>