<?php
session_start();
error_reporting(E_ALL);

$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
$loc = $http_port != '80' ? $http_host.":".$http_port : $http_host;

if(empty($_SESSION['data_user'])) {
    header("Location: http://".$http_host."/loginadmin.php");
} else {
	if ($_SESSION['data_user']['jabatan'] == 'gudang') {
		header("Location: http://".$http_host."/gudang/dashboard_gudang.php");
	}
	if ($_SESSION['data_user']['jabatan'] == 'kasir') {
		header("Location: http://".$http_host."/kasir/dashboard_kasir.php");
	}
	if ($_SESSION['data_user']['jabatan'] == 'manager') {
		header("Location: http://".$http_host."/manager/dashboard_manager.php");
	}
	if ($_SESSION['data_user']['jabatan'] == 'dapur') {
		header("Location: http://".$http_host."/dapur/dashboard_dapur.php");
	}
    
}
