<?php
session_start();
//utk kebutuhan path ketika hapus gambar di folder
$root_folder =  $_SERVER['DOCUMENT_ROOT'];
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
include "../../config/koneksi.php";
$hapus = new Koneksi();
$id = $_POST['menu_id_delete'];
//sebelum dihapus ambil link gambar id menu dari database
$url_gambar = $hapus->select('menu','gambar',"id_menu= "."'".$_POST['menu_id_delete']."'");

//ambil value dari array gambar
foreach ($url_gambar as $key => $value) {
	$url_gambar = $value['gambar'];
}
//pisahkan url gambar menjadi 3 bagian
$ambil_nama_gambar = explode('/', $url_gambar);

//hapus gambar di folder
unlink($root_folder.'/'.$ambil_nama_gambar[1].'/'.$ambil_nama_gambar[2]);
$hasil = $hapus->hapus('menu','id_menu',$_POST['menu_id_delete']);

if ($hasil > 0) {
	$_SESSION['error'] = 'Berhasil Hapus Menu';
	header("Location: http://".$http_host."/gudang/listmenu.php");
} else {
	$_SESSION['error'] = 'Gagal Hapus Menu';
	header("Location: http://".$http_host."/gudang/listmenu.php");
}