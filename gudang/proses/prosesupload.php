<?php
session_start();
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
include "../../config/koneksi.php";
$insertmenu = new Koneksi();
//set variable ke null
$error_nama_menu = $error_harga = $error_deskripsi = $erro_stok = "";
$nama_menu = $harga = $deskripsi = $stok = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
	if ((empty($_POST['nama_menu'])) && (empty($_POST['harga'])) && (empty($_POST['deskripsi'])) && (empty($_POST['stok']))) 
	{
		$error_nama_menu = "Nama Menu Tidak Boleh Kosong";
		$error_harga = 'Harga Tidak Boleh Kosong';
		$error_deskripsi = 'Deskripsi Produk Tidak Boleh Kosong';
		$erro_stok = 'Stok Tidak Boleh Kosong';
		$_SESSION['error'] = 'Inputan Tidak Boleh Kosong, Stok dan Harga Hanya Angka';
		header("Location: http://".$http_host."/gudang/tambahmenu.php");
	} 
	else 
	{
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["gambarmenu"]["name"]);
		$extension = end($temp);
		$path_uploads = $http_host.'/uploads/'.basename($_FILES["gambarmenu"]["name"]);

		if ((($_FILES["gambarmenu"]["type"] == "image/gif")
		|| ($_FILES["gambarmenu"]["type"] == "image/jpeg")
		|| ($_FILES["gambarmenu"]["type"] == "image/jpg")
		|| ($_FILES["gambarmenu"]["type"] == "image/pjpeg")
		|| ($_FILES["gambarmenu"]["type"] == "image/x-png")
		|| ($_FILES["gambarmenu"]["type"] == "image/png"))
		&& ($_FILES["gambarmenu"]["size"] < 200000)
		&& in_array($extension, $allowedExts)) 
		{
		  if ($_FILES["gambarmenu"]["error"] > 0) 
		  {
		    // echo "Return Code: " . $_FILES["gambarmenu"]["error"] . "<br>";
		    $_SESSION['error'] = $_FILES['gambarmenu']['error'];
		  	header("Location: http://".$http_host."/gudang/tambahmenu.php");
		  } 
		  else 
		  {
		    if (file_exists("../../uploads/" . $_FILES["gambarmenu"]["name"])) 
		    {
		      // echo $_FILES["gambarmenu"]["name"] . " Gambar Sudah ada. ";
		      	$_SESSION['error'] = $_FILES['gambarmenu']['name'].' Gambar Sudah Ada';
		  		header("Location: http://".$http_host."/gudang/tambahmenu.php");
		    }
		     else 
		     {
		      	move_uploaded_file($_FILES["gambarmenu"]["tmp_name"],
		      	"../../uploads/" . $_FILES["gambarmenu"]["name"]);
		      
		      //insert ke database
		      	$insertmenu->insert('menu',array(
		      						'nama_menu' => $_POST['nama_menu'],
		      						'harga' => $_POST['harga'],
		      						'gambar' => $path_uploads,
		      						'deskripsi' => $_POST['deskripsi'],
		      						'stok' => $_POST['stok'],
		      						'id_jenis' => $_POST['jenis_menu']
		      		)
		      	);
		  //    	$_SESSION['error'] = 'Berhasil Input Menu Baru';
				// header("Location: http://".$http_host."/gudang/listmenu.php"); 
		    }

		    $_SESSION['error'] = 'Berhasil Input Menu Baru';
			header("Location: http://".$http_host."/gudang/listmenu.php");
		  }
		} 
		else {
		  // echo "Invalid file";
		  $_SESSION['error'] = 'Gambar Kosong , File Terlalu Besar atau Ekstensi Gambar Tidak di Izinkan';
		  header("Location: http://".$http_host."/gudang/tambahmenu.php");
		}
		// $_SESSION['error'] = 'Berhasil Input Menu Baru';
		// header("Location: http://".$http_host."/gudang/listmenu.php");
	} 
} else {
	$_SESSION['error'] = 'Gagal Input Data';
	header("Location: http://".$http_host."/gudang/tambahmenu.php");
}

