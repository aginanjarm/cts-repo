<?php
session_start();
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
include "../../config/koneksi.php";
$updatestok = new Koneksi();

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	if (!empty($_POST['stok'])) {
		$query = $updatestok->update('menu',array('stok' => $_POST['stok']),'id_menu='.'"'.$_POST['id_menu'].'"');
		if ($query > 0) {
			$_SESSION['error'] = 'Stok Menu Berhasil Di Update';
			header("Location: http://".$http_host."/gudang/listmenu.php");
		}
	}
}
else
{
	$_SESSION['error'] = 'Gagal Update Stok';
	header("Location: http://".$http_host."/gudang/listmenu.php");
}