<?php
session_start();
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
include "../config/koneksi.php";
if (!empty($_SESSION['data_user'])) {
$menu = new Koneksi();
$hasilmenu = $menu->select('menu','*');
include "../lib/templates/header.php"; ?>
<!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../gudang/dashboard_gudang.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Cafe Teras Sosis
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['data_user']['username'];?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="../lib/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <!-- <img src="img/avatar3.png" class="img-circle" alt="User Image" /> -->
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $_SESSION['data_user']['username'];?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="/gudang/dashboard_gudang.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>

                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Menu</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="listmenu.php"><i class="fa fa-angle-double-right"></i> List Menu</a></li>
                                <li><a href="tambahmenu.php"><i class="fa fa-angle-double-right"></i> Tambah Menu</a></li>
                                <li><a href="update_stok_menu.php"><i class="fa fa-angle-double-right"></i> Update Stok Menu</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                        
                        <ol class="breadcrumb">
                        <li><a href="dashboard_gudang.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Update Stok menu</li>
                    </ol>
                    <h1>&nbsp;</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                        
                    <!-- Small boxes (Stat box) -->
                    <div class="row">

                        <div class="col-md-12">
                            <!-- <div class="box"> -->
                                
                                <div class="box-body">
                                    <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Update Stok Menu</h3>
                                </div><!-- /.box-header -->
                                <?php 
                                //tampilkan error message 
                                if ((($_SESSION['error']))) {
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $_SESSION['error'];?></div>
                                    <?php unset($_SESSION['error']);
                                }
                                ?>
                                <!-- form start -->
                                <form role="form" method="post" action="proses/updatestok.php" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Nama Menu</label>
                                            <select class="form-control" name="id_menu">
                                                <?php foreach($hasilmenu as $key => $value):?>
                                                <option value="<?php echo $value['id_menu'];?>"><?php echo $value['nama_menu'];?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi">Stok</label>
                                            <span class="error">* <?php echo $error_stok;?></span>
                                            <input type="number" class="form-control" id="stok" name="stok" placeholder=" Stok Produk" required oninvalid="this.setCustomValidity('Input Stok Tidak Boleh Kosong dan Hanya Di isi dengan Angka')" oninput="setCustomValidity('')">
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                </form>
                            </div>
                                </div><!-- /.box-body -->
                                
                            <!-- </div> --><!-- /.box -->
                        </div>                        
                    </div>
                </section>      
            </aside>
        </div><!-- ./wrapper -->
        
        <?php 
//include footer dari template
include "../lib/templates/footer.php"; 
//tutup else dari cek apakah session login ada
} 

else {
    header("Location: http://".$http_host."/loginadmin.php");
}
?>

