<?php
session_start();
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
include "../config/koneksi.php";
//cek apakah user punya session login 
if (!empty($_SESSION['data_user'])) {
$konek = new Koneksi;
$menus = $konek->hasMany('menu','*', 'jenis', 'id_jenis', 'id_jenis');

    
include "../lib/templates/header.php"; ?>
<!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Cafe Teras Sosis
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['data_user']['username'];?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="../lib/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <!-- <img src="img/avatar3.png" class="img-circle" alt="User Image" /> -->
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $_SESSION['data_user']['username'];?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="/gudang/dashboard_gudang.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>

                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Menu</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="listmenu.php"><i class="fa fa-angle-double-right"></i> List Menu</a></li>
                                <li><a href="tambahmenu.php"><i class="fa fa-angle-double-right"></i> Tambah Menu</a></li>
                                <li><a href="update_stok_menu.php"><i class="fa fa-angle-double-right"></i> Update Stok Menu</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                        
                        <ol class="breadcrumb">
                        <li><a href="dashboard_gudang.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">list menu</li>
                    </ol>
                    <h1>&nbsp;</h1>
                </section>

                <!-- Main content -->
                <section class="content">
                        
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <?php 
                                //tampilkan error message 
                                if ((($_SESSION['error']))) {
                                    ?>
                                    <div class="alert alert-success alert-dismissable errorna">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $_SESSION['error'];?></div>
                                    <?php unset($_SESSION['error']);
                                }
                                ?>
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">List Menu</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th style="width: 10px">No</th>
                                            <th>Nama Menu</th>
                                            <th>Harga</th>
                                            <th>Gambar</th>
                                            <th>Jenis</th>
                                            <th>stok</th>
                                            <th style="width:220px;">Aksi</th>
                                        </tr>
                                        <?php 
                                            //looping data menus 
                                        $i=1;
                                        foreach($menus as $key => $value):?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value['nama_menu'];?></td>
                                            <td><?php echo $value['harga'];?></td>
                                            <td>
                                                <!-- <button data-toggle="modal" data-target="#viewGambar" class="btn btn-info" onclick="viewGambarFunc("<?php echo $value['gambar'];?>")">

                                                </button> -->
                                                <button class="btn btn-default">
                                                    <img src="<?php echo 'http://'.$value['gambar'];?>" style="width:50px;height:50px;">
                                                </button>
                                            </td>
                                            <td><?php echo $value['nama_jenis'];?></td>
                                            <td><?php echo $value['stok'];?></td>
                                            <td>
                                                <!-- <button class="btn btn-primary">Detail</button>
                                                <button class="btn btn-info">Edit</button> -->
                                                <!-- <button class="btn btn-danger" onclick="hapusMenu(<?php echo $value['id_menu'];?>)">Hapus</button> -->
                                                <form method="post" action="proses/hapusmenu.php">
                                                <input type="hidden" name="menu_id_delete" value="<?php echo $value['id_menu']; ?>">
                                                <button class="btn btn-danger" type="submit"> Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php 
                                    $i++;
                                    endforeach;?>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">&laquo;</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">&raquo;</a></li>
                                    </ul>
                                </div> -->
                            </div><!-- /.box -->
                        </div>                        
                    </div>
                </section>      
            </aside>
        </div><!-- ./wrapper -->
<!--Modal gambar-->
<div class="modal fade" id="viewGambar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Gambar</h4>
      </div>
      <div class="modal-body menugambar">
        <img id="gambar_id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">tutup</button>
        <!-- <input type="submit" class="btn btn-primary" value="Update"> -->
      </div>
    </div>
  </div>
</div>
        
<script type="text/javascript">
function viewGambarFunc(gambar) {
    $('#gambar_id').attr('src') =gambar;
    console.log(gambar);
}
//hapus menu dari page listmenu admin
// function hapusMenu(id) {
//     $.ajax({
//         type : 'POST',
//         url : 'proses/hapusmenu.php',
//         dataType : 'json',
//         data : 'menu_id_delete='+id,
//         success : function(data) {
//             if (data.error ==0) {
//                 $('.errorna').html(data.message);
//                 alert('berhasil Hapus data');
//                 console.log(data);
//             } else {
//                 // console.log(data.message);
//                 $('.errorna').html(data.message);
//                 window.Location.href = data.redirect;
//             }
//         }
//     });
//     return false;

// }   

</script>        
        <?php 
//include footer dari template
include "../lib/templates/footer.php"; 
//tutup else dari cek apakah session login ada
} 

else {
    header("Location: http://".$http_host."/loginadmin.php");
}
?>

