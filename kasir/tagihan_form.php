<?php
// session_start();
include "../lib/templates/header.php";
include "../config/koneksi.php";
$no_meja = $_GET['no'];
$konek = new Koneksi;
$pesanan = $konek->select('pesanan','*','id_meja='.$no_meja);
$meja = $konek->select('meja','*','no_meja='.$no_meja);
$pelanggan = $konek->select('pelanggan','nama_pelanggan','id_pelanggan='.$pesanan[0]['id_pelanggan']);
// var_dump($pelanggan);die;
?>
<header class="header">
    <a href="/kasir/" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Cafe Teras Sosis
    </a>
    <nav class="navbar navbar-static-top" role="navigation"></nav>
</header>
<div class="container">
	<div class="clear"></div>
	<div class="row">
		<h3>Pemesanan Meja <?php echo $no_meja?></h3>
		<h3>Atas Nama: <strong><?php echo $pelanggan[0]['nama_pelanggan']?></strong></h3>
	</div>
	<div class="row">
		<form action="proses_tagihan.php" method="POST"> 
		<table class="table table-hover">
		  <tr class="active">
			  <th>No</th>
			  <th>Nama</th>
			  <th>Qty</th>
			  <th>Harga @</th>
			  <th>Subtotal</th>
		  </tr>
		  <?php 
		  $no = 1;
		  $total = 0;
		  foreach ($pesanan as $key => $value): 
		  $menu = $konek->select('menu','*','id_menu='.$value['id_menu']);
		  ?>
		  <tr>
			  <td><?php echo $no;?></td>
			  <td><?php echo $menu[0]['nama_menu'];?></td>
			  <td><?php echo $pesanan[$no - 1]['quantity'];?></td>
			  <td><?php echo $menu[0]['harga'];?></td>
			  <td><?php echo $pesanan[$no - 1]['sub_total'];?></td>
		  </tr>
		  <?php 
		  $total += $pesanan[$no - 1]['sub_total'];
		  $no++;
		  endforeach; ?>
		  <tr>
		  	<td>&nbsp</td>
		  	<td>&nbsp</td>
		  	<td>&nbsp</td>
		  	<td>Total</td>
		  	<td><?php echo $total;?></td>
		  </tr>
		</table>
		<input type="submit" class="btn btn-primary pull-right" value="Tutup"  />
		<input type="submit" class="btn btn-primary pull-right" value="Cetak" style="margin-right:10px" />
		</form>
	</div>
</div>
<?php 
include "../lib/templates/footer.php";
?>