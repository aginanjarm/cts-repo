<?php
session_start();
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
include "../config/koneksi.php";
if (!empty($_SESSION['data_user'])) {
	
include "../lib/templates/header.php"; ?>
<!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Cafe Teras Sosis
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['data_user']['username'];?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="../lib/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
        	<!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <!-- <img src="img/avatar3.png" class="img-circle" alt="User Image" /> -->
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $_SESSION['data_user']['username'];?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="/kasir/dashboard_kasir.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="/kasir/tagihan.php">
                                <i class="fa fa-th"></i> <span>Tagihan</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
					<h3>Selamat Datang <strong><?php echo $_SESSION['data_user']['username'];?></strong></h3>
                    <!-- Small boxes (Stat box) -->
                    <!-- <div class="row"> -->
                    	<!-- <div class="container"> -->
							<div class="clear"></div>
							<div class="row dapur">
							
							</div>
						<!-- </div> -->
                    <!-- </div> -->
                </section>   	
            </aside>
        </div><!-- ./wrapper -->
        
        <?php 
//include footer dari template
include "../lib/templates/footer.php"; 
//tutup else dari cek apakah session login ada
} 

else {
	header("Location: http://".$http_host."/loginadmin.php");
}
?>