<?php
$http_host = $_SERVER['HTTP_HOST'];
$http_port = $_SERVER['SERVER_PORT'];
// $loc = $http_port != '80' ? $http_host.":".$http_port : $http_host;
$css = "http://".$http_host."/assets/css/";
$js = "http://".$http_host."/assets/js/";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cafe Teras Sosis</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo $css?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo $css?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo $css?>ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo $css?>AdminLTE.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css?>custom.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css?>teras-sosis.css" rel="stylesheet" type="text/css" />
        <script src="<?php //echo $js?>jquery.min.js"></script>
        <script src="<?php //echo $js?>bootstrap.min.js" type="text/javascript"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">